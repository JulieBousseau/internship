// const chartData = 'https://drive.google.com/uc?export=download&id=1nLQdyur_NZhqEz85ZfGJFbvlXtu1NBSJXWOPAcsNHyg';
const chartData = './data/tests/test_system_sentiments.csv';

// parse the file from CSV to JSON
d3.csv(chartData)
    .then(function (datapoints) {
        console.log(datapoints);

        let listSystems = [];
        let listTotalComments = [];
        let listNegative = [];
        let listNeutral = [];
        let listPositive = [];

        for (i = 0; i < datapoints.length; i++) {
            listSystems.push(datapoints[i].system);
            listTotalComments.push(datapoints[i].total_comments);
            listNegative.push(datapoints[i].negative);
            listNeutral.push(datapoints[i].neutral);
            listPositive.push(datapoints[i].positive);
        }

        // setup
        const data = {
            datasets: [
                {
                    data: [84, 28, 57],
                    backgroundColor: [
                        'rgba(255,99,132,0.5)',
                        'rgba(255,159,64,0.5)',
                        'rgba(255,205,86,0.5)',
                        'rgba(75,192,192,0.5)',
                        'rgba(54,162,235,0.5)',
                    ],
                    label: 'Dataset 1',
                },
            ],
            labels: ['Negative', 'Neutral', 'Positive'],
        };

        // config
        const config = {
            type: 'pie',
            data,
            // options: {
            //     scales: {
            //         y: {
            //             beginAtZero: true
            //         }
            //     }
            // }
        };

        // render init block
        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    })
    .catch(function (e) {
        console.error('CKC : ' + e);
    });
