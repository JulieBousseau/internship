const listFiles = [
    './data/charts/sqlite_years_sentiments.csv',
    './data/charts/mongodb_years_sentiments.csv',
    './data/charts/redis_years_sentiments.csv',
    './data/charts/oracledb_years_sentiments.csv',
    './data/charts/elasticsearch_years_sentiments.csv',
    './data/charts/ibmdb_years_sentiments.csv',
    './data/charts/postgresql_years_sentiments.csv',
    './data/charts/mysql_years_sentiments.csv',
];

for (i = 0; i < listFiles.length; i++) {
    let currentFile = listFiles[i];
    let system = getStringBetween(currentFile, './data/charts/', '_years_sentiments.csv');

    d3.csv(currentFile)
        .then(function (datapoints) {
            let listYears = [];
            let listNegative = [];
            let listNeutral = [];
            let listPositive = [];

            for (j = 0; j < datapoints.length; j++) {
                let year = datapoints[j].year;
                let negative = datapoints[j].negative;
                let neutral = datapoints[j].neutral;
                let positive = datapoints[j].positive;

                listYears.push(year);
                listNegative.push(negative);
                listNeutral.push(neutral);
                listPositive.push(positive);
            }

            let config = se_dataConfig(
                listYears,
                listNegative,
                listNeutral,
                listPositive
            );

            canvas = se_createTemplate(system);
            se_createChart(canvas, config);
        })

        .catch(function (e) {
            console.error('CKC : ' + e);
        });
}

function se_dataConfig(
    listYears,
    listNegative,
    listNeutral,
    listPositive
) {
    const data = {
        labels: listYears,
        datasets: [
            {
                label: 'negative',
                data: listNegative,
                borderColor: 'rgba(255, 99, 132, 1)',
            },

            {
                label: 'neutral',
                data: listNeutral,
                borderColor: 'rgba(255, 255, 99, 1)',
            },

            {
                label: 'positive',
                data: listPositive,
                borderColor: 'rgba(99, 255, 99, 1)',
            },
        ],
    };

    return se_config(data);
}

function se_config(data) {
    return {
        type: 'line',
        data,
        options: {
            scales: {
                x: {
                    title: {
                        display: true,
                        text: 'year',
                    },
                },
                y: {
                    title: {
                        display: true,
                        text: 'number of comments',
                    },
                },
            },
        },
    };
}

function se_createTemplate(system) {
    let canvasId = 'chartSentimentsEvolution-';

    let h3 = document.createElement('h3');
    h3.innerHTML = capitalizeFirstLetter(system);
    let canvas = document.createElement('canvas');
    canvas.setAttribute('id', canvasId + system);
    let div = document.createElement('div');
    div.setAttribute('class', 'sentiEvo');

    let section = document.getElementById('sectSentimentsEvolution');
    div.appendChild(h3);
    div.appendChild(canvas);
    section.appendChild(div);

    return canvasId + system;    
}

function se_createChart(canvas, config) {
    new Chart(document.getElementById(canvas), config);
}

function getStringBetween(str, start, end) {
    const result = str.match(new RegExp(start + "(.*)" + end));

    return result[1];
}
