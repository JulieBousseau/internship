const chartDataOrigin = './data/charts/system_sentiments.csv';

d3.csv(chartDataOrigin)
    .then(function (datapoints) {
        let listSystems = [];
        let listTotal = [];
        for (i = 0; i < datapoints.length; i++) {
            let system = datapoints[i].system;
            let total = datapoints[i].total_comments;

            listSystems.push(system);
            listTotal.push(total);
        }

        let config = ori_dataConfig(listSystems, listTotal);

        canvas = ori_createTemplate();
        ori_createChart(canvas, config);
    })

    .catch(function (e) {
        console.error('CKC : ' + e);
    });

function ori_dataConfig(listSystems, listTotal) {
    const data = {
        datasets: [
            {
                data: listTotal,
                backgroundColor: [
                    'rgb(3, 55, 84)',
                    'rgb(7, 172, 79)',
                    'rgb(216, 44, 32)',
                    'rgb(237, 27, 36)',
                    'rgb(240, 191, 26)',
                    'rgb(85, 211, 210)',
                    'rgb(47, 96, 145)',
                    'rgb(228, 142, 0)',
                    'rgb(170, 31, 36)',
                    'rgb(176, 32, 49)',
                ],
                /* sqlite:rgb(3, 55, 84)
                mongo: rgb(7, 172, 79)
                redis: rgb(216, 44, 32)
                oracle: rgb(237, 27, 36)
                elastic: rgb(240, 191, 26)
                ibm: rgb(85, 211, 210)
                postgre: rgb(47, 96, 145)
                mysql: rgb(228, 142, 0)
                micro sql server: rgb(170, 31, 36)
                micro access: rgb(176, 32, 49)
                */
            },
        ],
        labels: listSystems,
    };

    return ori_config(data);
}

function ori_config(data) {
    return {
        type: 'pie',
        data,
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Repartition of the comments'
                }
            }
        }
    };
}

function ori_createTemplate() {
    let canvasId = 'chartDataOrigin';

    // let h3 = document.createElement('h3');
    // h3.innerHTML = capitalizeFirstLetter(system);
    let canvas = document.createElement('canvas');
    canvas.setAttribute('id', canvasId);
    let div = document.createElement('div');
    div.setAttribute('class', 'dataOrigin');

    let section = document.getElementById('dataOriginComments');
    // div.appendChild(h3);
    div.appendChild(canvas);
    section.appendChild(div);

    return canvasId;
}

function ori_createChart(canvas, config) {
    new Chart(document.getElementById(canvas), config);
}
