// const listFiles = [
//     './data/charts/sqlite_years_sentiments.csv',
//     './data/charts/mongodb_years_sentiments.csv',
//     './data/charts/redis_years_sentiments.csv',
//     './data/charts/oracledb_years_sentiments.csv',
//     './data/charts/elasticsearch_years_sentiments.csv',
//     './data/charts/ibmdb_years_sentiments.csv',
//     './data/charts/postgresql_years_sentiments.csv',
//     './data/charts/mysql_years_sentiments.csv',
// ];

const listFilesSentimentsComparison = [
    './data/charts/postgresql_years_sentiments.csv',
    './data/charts/mysql_years_sentiments.csv',
];

(function () {
    'use strict';
    $(document).ready(function () {
        sCoY_runChartJs(listFilesSentimentsComparison);
        sCoY_handleForm($('#form-dbms'));
    });
})();

function sCoY_handleForm(formId) {
    $(formId).submit(function (event) {
        event.preventDefault(); // Stop the browser from submitting the form

        let formData = $(formId).serializeArray();

        let checkboxes = [];
        let radio;

        for (data of formData) {
            if (data.name == 'checkbox') {
                checkboxes.push(data);
            } else {
                radio = data;
            }
        }

        console.log(radio);

        if (checkboxes.length != 2) {
            popupInfo('accept', 'Please select 2 DBMS to compare');
        } else {
            $('.alert').remove();
            listFilesDbms = [
                './data/charts/' +
                    checkboxes[0]['value'] +
                    '_years_sentiments.csv',
                './data/charts/' +
                    checkboxes[1]['value'] +
                    '_years_sentiments.csv',
            ];
            sCoY_runChartJs(listFilesDbms, radio['value']);
        }
    });
}

function sCoY_runChartJs(tableData, radio) {
    Promise.all([d3.csv(tableData[0]), d3.csv(tableData[1])])
        .then(function (datapoints) {
            let file1 = datapoints[0];
            let file2 = datapoints[1];

            let system1 = getStringBetween(
                tableData[0],
                './data/charts/',
                '_years_sentiments.csv'
            );

            let system2 = getStringBetween(
                tableData[1],
                './data/charts/',
                '_years_sentiments.csv'
            );

            let listYears1 = [];
            let listNegative1 = [];
            let listNeutral1 = [];
            let listPositive1 = [];

            let listYears2 = [];
            let listNegative2 = [];
            let listNeutral2 = [];
            let listPositive2 = [];

            for (i = 0; i < file1.length; i++) {
                let year = file1[i].year;
                let negative = parseInt(file1[i].negative);
                let neutral = parseInt(file1[i].neutral);
                let positive = parseInt(file1[i].positive);

                let total = negative + neutral + positive;

                let percentNegative = (
                    (negative / total) *
                    100
                ).toFixed(1);
                let percentNeutral = (
                    (neutral / total) *
                    100
                ).toFixed(1);
                let percentPositive = (
                    (positive / total) *
                    100
                ).toFixed(1);

                listYears1.push(year);
                listNegative1.push(percentNegative);
                listNeutral1.push(percentNeutral);
                listPositive1.push(percentPositive);
            }

            for (j = 0; j < file2.length; j++) {
                let year = file2[j].year;
                let negative = parseInt(file2[j].negative);
                let neutral = parseInt(file2[j].neutral);
                let positive = parseInt(file2[j].positive);

                let total = negative + neutral + positive;

                let percentNegative = (
                    (negative / total) *
                    100
                ).toFixed(1);
                let percentNeutral = (
                    (neutral / total) *
                    100
                ).toFixed(1);
                let percentPositive = (
                    (positive / total) *
                    100
                ).toFixed(1);

                listYears2.push(year);
                listNegative2.push(percentNegative);
                listNeutral2.push(percentNeutral);
                listPositive2.push(percentPositive);
            }

            let config = sCoY_dataConfig(
                listYears1,
                listNegative1,
                listNeutral1,
                listPositive1,
                listYears2,
                listNegative2,
                listNeutral2,
                listPositive2,
                system1,
                system2,
                radio
            );

            canvas = sCoY_createTemplate(system1, system2);
            sCoY_createChart(canvas, config);
        })

        .catch(function (e) {
            console.error('CKC : ' + e);
        });
}

function sCoY_dataConfig(
    listYears1,
    listNegative1,
    listNeutral1,
    listPositive1,
    listYears2,
    listNegative2,
    listNeutral2,
    listPositive2,
    system1,
    system2,
    radio
) {
    let data;

    if (radio == 'coms-all') {
        data = {
            labels: listYears1,
            datasets: [
                {
                    label: system1 + ' negative',
                    data: listNegative1,
                    borderColor: 'rgba(255, 99, 132, 1)',
                },

                {
                    label: system1 + ' neutral',
                    data: listNeutral1,
                    borderColor: 'rgba(255, 255, 99, 1)',
                },

                {
                    label: system1 + ' positive',
                    data: listPositive1,
                    borderColor: 'rgba(99, 255, 99, 1)',
                },

                {
                    label: system2 + ' negative',
                    data: listNegative2,
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderDash: [10, 10],
                },

                {
                    label: system2 + ' neutral',
                    data: listNeutral2,
                    borderColor: 'rgba(255, 255, 99, 1)',
                    borderDash: [10, 10],
                },

                {
                    label: system2 + ' positive',
                    data: listPositive2,
                    borderColor: 'rgba(99, 255, 99, 1)',
                    borderDash: [10, 10],
                },
            ],
        };
    } else if (radio == 'coms-positive') {
        data = {
            labels: listYears1,
            datasets: [
                {
                    label: system1 + ' positive',
                    data: listPositive1,
                    borderColor: 'rgba(99, 255, 99, 1)',
                },

                {
                    label: system2 + ' positive',
                    data: listPositive2,
                    borderColor: 'rgba(99, 255, 99, 1)',
                    borderDash: [10, 10],
                },
            ],
        };
    } else if (radio == 'coms-neutral') {
        data = {
            labels: listYears1,
            datasets: [
                {
                    label: system1 + ' neutral',
                    data: listNeutral1,
                    borderColor: 'rgba(255, 255, 99, 1)',
                },

                {
                    label: system2 + ' neutral',
                    data: listNeutral2,
                    borderColor: 'rgba(255, 255, 99, 1)',
                    borderDash: [10, 10],
                },
            ],
        };
    } else {
        data = {
            labels: listYears1,
            datasets: [
                {
                    label: system1 + ' negative',
                    data: listNegative1,
                    borderColor: 'rgba(255, 99, 132, 1)',
                },

                {
                    label: system2 + ' negative',
                    data: listNegative2,
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderDash: [10, 10],
                },
            ],
        };
    }

    return sCoY_config(data, system1, system2);
}

function sCoY_config(data, system1, system2) {
    return {
        type: 'line',
        data,
        options: {
            scales: {
                x: {
                    title: {
                        display: true,
                        text: 'year',
                    },
                },

                y: {
                    title: {
                        display: true,
                        text: 'comments (%)',
                        beginAtZero: true
                    },
                },
            },

            plugins: {
                title: {
                    display: true,
                    text:
                        'Comparison between ' +
                        system1 +
                        ' and ' +
                        system2,
                },
            },
        },
    };
}

function sCoY_createTemplate(system1, system2) {
    let canvasId = 'chartSentimentsComparisonYear';

    // let h3 = document.createElement('h3');
    // h3.innerHTML = capitalizeFirstLetter(
    //     'Comparison between ' + system1 + ' and ' + system2
    // );

    let canvas = document.createElement('canvas');
    canvas.setAttribute('id', canvasId);
    let div = document.createElement('div');
    div.setAttribute('id', 'sentiCompaYear');

    let section = document.getElementById(
        'sectSentimentsComparisonYear'
    );

    if (document.contains(document.getElementById(canvasId))) {
        document.getElementById(canvasId).remove();
    }

    if (
        document.contains(document.getElementById('sentiCompaYear'))
    ) {
        document.getElementById('sentiCompaYear').remove();
    }

    // div.appendChild(h3);
    div.appendChild(canvas);
    section.appendChild(div);

    return canvasId;
}

let myChart = null;

function sCoY_createChart(canvas, config) {
    if (myChart != null) {
        myChart.destroy();
    }

    myChart = new Chart(document.getElementById(canvas), config);
}

function popupInfo(info, content) {
    let text = content;
    switch (info) {
        case 'accept':
            text =
                "<i class='fas fa-exclamation-triangle'></i>" + text;
            break;

        case 'error':
            text = "<i class='fas fa-times-circle'></i>" + text;
            break;

        case 'passed':
            text = "<i class='fas fa-check-circle'></i>" + text;
            break;

        default:
            break;
    }
    $('.alertHolder').append(
        '<div class="alert ' + info + '"><p>' + content + '</p></div>'
    );

    setTimeout(function () {
        $('.alert').remove();
    }, 8000);
}
