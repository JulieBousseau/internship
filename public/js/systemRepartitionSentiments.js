const chartDataRepartitionSentiments =
    './data/charts/system_sentiments.csv';

d3.csv(chartDataRepartitionSentiments)
    .then(function (datapoints) {

        for (i = 0; i < datapoints.length; i++) {
            let total = datapoints[i].total_comments;

            if (total != 0) {
                let system = datapoints[i].system;
                let negative = datapoints[i].negative;
                let neutral = datapoints[i].neutral;
                let positive = datapoints[i].positive;

                let config = rep_dataConfig(
                    system,
                    negative,
                    neutral,
                    positive
                );

                canvas = rep_createTemplate(system);
                rep_createChart(canvas, config);
            }
        }
    })

    .catch(function (e) {
        console.error('CKC : ' + e);
    });

function rep_dataConfig(system, negative, neutral, positive) {
    const data = {
        datasets: [
            {
                data: [negative, neutral, positive],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 255, 99, 1)',
                    'rgba(99, 255, 99, 1)',
                ],
                label: 'Sentiment repartition for ' + system,
            },
        ],
        labels: ['Negative', 'Neutral', 'Positive'],
    };

    return rep_config(data);
}

function rep_config(data) {
    return {
        type: 'pie',
        data,
    };
}

function rep_createTemplate(system) {
    let canvasId = 'chartRepartitionSentiment-';

    let h3 = document.createElement('h3');
    h3.innerHTML = capitalizeFirstLetter(system);
    let canvas = document.createElement('canvas');
    canvas.setAttribute('id', canvasId + system);
    let div = document.createElement('div');
    div.setAttribute('class', 'repartitionSentiment');

    let section = document.getElementById(
        'sectRepartitionSentiments'
    );
    div.appendChild(h3);
    div.appendChild(canvas);
    section.appendChild(div);

    return canvasId + system;
}

function rep_createChart(canvas, config) {
    new Chart(document.getElementById(canvas), config);
}
