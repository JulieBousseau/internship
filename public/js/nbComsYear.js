const chartNbComsYear = './data/charts/dates_nb.csv';

d3.csv(chartNbComsYear)
    .then(function (datapoints) {
        let listYears = [];
        let listSystems = [];
        let listNbComments = [];
        let listFullNbComments = [];

        // Get the years
        let keys = Object.keys(datapoints[0]);
        for (i = 0; i < keys.length; i++) {
            let yearInt = parseInt(keys[i]);
            if (!Number.isNaN(yearInt)) {
                listYears.push(yearInt);
            }
        }

        // Get the data
        for (i = 0; i < datapoints.length; i++) {
            let system = datapoints[i].system;

            for (j = 0; j < listYears.length; j++) {
                year = listYears[j];
                let nbComments = datapoints[i][year];
                listNbComments.push(nbComments);
            }

            listSystems.push(system);
            listFullNbComments.push(listNbComments);

            listNbComments = [];
        }

        let config = nbCY_dataConfig(
            listSystems,
            listFullNbComments,
            listYears
        );

        canvas = nbCY_createTemplate();
        nbCY_createChart(canvas, config);
    })

    .catch(function (e) {
        console.error('CKC : ' + e);
    });

function nbCY_dataConfig(listSystems, listFullNbComments, listYears) {
    const data = {
        labels: listYears,
        datasets: [
            {
                label: listSystems[0],
                data: listFullNbComments[0],
                borderColor: 'rgb(237, 27, 36)',
            },

            {
                label: listSystems[1],
                data: listFullNbComments[1],
                borderColor: 'rgb(228, 142, 0)',
            },

            {
                label: listSystems[2],
                data: listFullNbComments[2],
                borderColor: 'rgb(3, 55, 84)',
            },

            {
                label: listSystems[3],
                data: listFullNbComments[3],
                borderColor: 'rgb(170, 31, 36)',
            },

            {
                label: listSystems[4],
                data: listFullNbComments[4],
                borderColor: 'rgb(7, 172, 79)',
            },

            {
                label: listSystems[5],
                data: listFullNbComments[5],
                borderColor: 'rgb(240, 191, 26)',
            },

            {
                label: listSystems[6],
                data: listFullNbComments[6],
                borderColor: 'rgb(176, 32, 49)',
            },

            {
                label: listSystems[7],
                data: listFullNbComments[7],
                borderColor: 'rgb(216, 44, 32)',
            },

            {
                label: listSystems[8],
                data: listFullNbComments[8],
                borderColor: 'rgb(47, 96, 145)',
            },

            {
                label: listSystems[9],
                data: listFullNbComments[9],
                borderColor: 'rgb(85, 211, 210)',
            },
        ],
    };

    return nbCY_config(data);
}

function nbCY_config(data) {
    return {
        type: 'line',
        data,
        options: {
            scales: {
                x: {
                    title: {
                        display: true,
                        text: 'year',
                    },
                },
                y: {
                    title: {
                        display: true,
                        text: 'number of comments',
                    },
                },
            },
        },
    };
}

function nbCY_createTemplate() {
    let canvasId = 'chartNbComsYear';

    // let h3 = document.createElement('h3');
    // h3.innerHTML = capitalizeFirstLetter(system);
    let canvas = document.createElement('canvas');
    canvas.setAttribute('id', canvasId);
    let div = document.createElement('div');
    div.setAttribute('class', 'nbComsYear');

    let section = document.getElementById('sectNbCommentsYears');
    // div.appendChild(h3);
    div.appendChild(canvas);
    section.appendChild(div);

    return canvasId;
}

function nbCY_createChart(canvas, config) {
    new Chart(document.getElementById(canvas), config);
}
