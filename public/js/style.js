(function () {
    'use strict';
    $(document).ready(function () {
        handleMainWidth();
        handleTopBarPadding();

        $(window).resize(handleMainWidth);
        $(window).resize(handleTopBarPadding);
    });
})();

function handleMainWidth() {
    if ($(window).width() > '1500') {
        const menuWidth = $('header').width();
        const mainMargin = 'calc(' + menuWidth + 'px + 2em)';
        // console.log(mainMargin);
        $('main').css('margin-left', mainMargin);
    }
	else {
		$('main').css('margin-left', 0);
	}
}

function handleTopBarPadding() {
    if ($(window).width() > '1500') {
		const menuWidth = $('header').width();
        const topBarPadding = 'calc(' + menuWidth + 'px + 3em)';
        $('.topBar').css('padding-left', topBarPadding);
    }
	else {
		$('.topBar').css('padding-left', '3em');
	}
}
