const chartSentimentsComparison =
    './data/charts/system_sentiments.csv';

d3.csv(chartSentimentsComparison)
    .then(function (datapoints) {
        let listSystems = [];
        let listNegative = [];
        let listNeutral = [];
        let listPositive = [];

        for (j = 0; j < datapoints.length; j++) {
            let system = datapoints[j].system;
            let negative = datapoints[j].negative;
            let neutral = datapoints[j].neutral;
            let positive = datapoints[j].positive;
            let total = datapoints[j].total_comments;

            let percentNegative = ((negative / total) * 100).toFixed(
                1
            );
            let percentNeutral = ((neutral / total) * 100).toFixed(1);
            let percentPositive = ((positive / total) * 100).toFixed(
                1
            );

            listSystems.push(system);
            listNegative.push(percentNegative);
            listNeutral.push(percentNeutral);
            listPositive.push(percentPositive);
        }

        let config = sC_dataConfig(
            listSystems,
            listNegative,
            listNeutral,
            listPositive
        );

        canvas = sC_createTemplate();
        sC_createChart(canvas, config);
    })

    .catch(function (e) {
        console.error('CKC : ' + e);
    });

function sC_dataConfig(
    listSystems,
    listNegative,
    listNeutral,
    listPositive
) {
    const data = {
        labels: listSystems,
        datasets: [
            {
                label: 'negative',
                data: listNegative,
                backgroundColor: 'rgba(255, 99, 132, 1)',
            },

            {
                label: 'neutral',
                data: listNeutral,
                backgroundColor: 'rgba(255, 255, 99, 1)',
            },

            {
                label: 'positive',
                data: listPositive,
                backgroundColor: 'rgba(99, 255, 99, 1)',
            },
        ],
    };

    return sC_config(data);
}

function sC_config(data) {
    return {
        type: 'bar',
        data,
        options: {
            scales: {
                x: {
                    stacked: true,
                    title: {
                        display: true,
                        text: 'system',
                    },
                },

                y: {
                    stacked: true,
                    max: 100,
                    title: {
                        display: true,
                        text: 'comments (%)',
                    },
                },
            },
        },
    };
}

function sC_createTemplate() {
    let canvasId = 'chartSentimentsComparison';

    // let h3 = document.createElement('h3');
    // h3.innerHTML = capitalizeFirstLetter(system);
    let canvas = document.createElement('canvas');
    canvas.setAttribute('id', canvasId);
    let div = document.createElement('div');
    div.setAttribute('class', 'sentiCompa');

    let section = document.getElementById('sectSentimentsComparison');
    // div.appendChild(h3);
    div.appendChild(canvas);
    section.appendChild(div);

    return canvasId;
}

function sC_createChart(canvas, config) {
    new Chart(document.getElementById(canvas), config);
}
