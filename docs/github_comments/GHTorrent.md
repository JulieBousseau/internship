# GHTorrent

## What is it?

GHTorrent is like a mirror of data stored in GitHub and is updated quite regularly. At the moment, all the data from GitHub from 2012 to 2019 are available on GHTorrent. This is a dataset provided by  Georgios Gousios, and his principal aim was to provide all the GitHub data but without the rate limitation of the GitHub API. All the data are stored in 2 databases : a MongoDB one in JSON format (18TB of compressed JSON data) and a MySQL one (6.5 billion rows of extracted data).

## Why don't we use it?

GHTorrent runs on Ruby 2. More precisely, to install it, we need Ruby 2.2. The problem is that Ruby 2.2 seems not to be supported with Ubuntu 20+. As I'm on Ubuntu 22, I did not manage to install it so I could not use GHTorrent. 

Moreover, the principal interest for us to use GHTorrent was to forger about te GitHub API rate limit. In fact, when not authenticated, there are only 60 requests per hour. But once authenticated, with a key, we can query up to 5000 times the API per hour. In our case, this is more than enough.

That is why we focused on using the GitHub API.