# Github API

> - Documentation:
>
> https://docs.github.com/en/rest/guides/getting-started-with-the-rest-api
>
> - Repo:
>
> https://docs.github.com/en/rest/repos

## Authentication

- Not connected: 60 queries/h
- Connected with an authentication token: 5000 queries/h

### Connection

- https://github.com/settings/tokens: set the token
- In Seafile: Internship/github_token.txt

### Use

```shell
$ curl -i -u your_username:$token https://api.github.com/users/octocat
```

<u>Ex.:</u>

```shell
$ curl -i -u JulieBousseau:ghp_azertyuiop123456789 https://api.github.com/users/octocat
```

:warning: Put it in a variable (i.e. ask the user)

### Another syntax

```shell
$ curl -i -H "Authorization: token ghp_azertyuiop123456789" \
	https://api.github.com/users/octocat
```

:white_check_mark: Prioritize this one

## Basic usage

### Get an user

```shell
$ curl -i -u JulieBousseau:ghp_azertyuiop123456789 https://api.github.com/users/octocat
```

### Get a repo

- Simply by its name

```shell
$ curl -i https://api.github.com/repos/twbs/bootstrap
```

- All of our repos

```shell
$ curl -i -u JulieBousseau:ghp_azertyuiop123456789  https://api.github.com/user/repos
```

- List for a user

```shell
$ curl -i https://api.github.com/users/octocat/repos
```

- List for an organization

```shell
$ curl -i https://api.github.com/orgs/octo-org/repos
```

- Only with a type of access (<u>ex.</u>: owner) 

```shell
$ curl -i "https://api.github.com/users/octocat/repos?type=owner"
```

### Conditionnal requests

When data hasn't changed, no need to retrieve it.

In the header:

```shell
> HTTP/2 200
> etag: W/"61e964bf6efa3bc3f9e8549e56d4db6e0911d8fa20fcd8ab9d88f13d513f26f0"
```

- 200: Worked
- etag: Fingerprint of the response
  - We can ask not to use our rate limit if the fingerprint hasn't changed

```shell
$ curl -i -H 'If-None-Match: "61e964bf6efa3bc3f9e8549e56d4db6e0911d8fa20fcd8ab9d88f13d513f26f0"' \
$    https://api.github.com/users/defunkt

> HTTP/2 304
```

- 304: The resource hasn't changed

## Misc

### Flags

- `-i`: Show header + data
  - `-I`: Only Header

- `-u`: Authentication
- `-H`: Add things in the header of the request

### Header

- Show when the token expires
  - `Github-Authentication-Token-Expiration`
- Show how many queries are left
  - `x-ratelimit-remaining`
- Show the status of the request
  - `HTML 2 200`, `HTML 2 401`...

## Focus on MongoDB and MySQL

### General

- Get all the repos with "MongoDB" (default: 30)

```shell
$ curl -i -H "Authorization: token ghp_azertyuiop123456789" \
	https://api.github.com/search/repositories?q=mongodb
```

https://api.github.com/search/repositories?q=mongodb

- Get 100 repos with "MySQL"

```shell
$ curl -i -H "Authorization: token ghp_azertyuiop123456789" \
	https://api.github.com/search/repositories?q=mysql&per_page=100
```

https://api.github.com/search/repositories?q=mysql&per_page=100

### Zoom on comments for one repo

- Get 10 comments for the repo "mysqljs/mysql"

```shell
$ curl -i -H "Authorization: token ghp_azertyuiop123456789" \
	https://api.github.com/repos/mysqljs/mysql/comments?per_page=10
```

https://api.github.com/repos/mysqljs/mysql/comments?per_page=10

:arrow_right: `repos/x/y/comments`: x & y come from the previous request (`items/nb/full_name`)

## Python

> - Library `requests`:
>
> https://realpython.com/python-requests/
>
> - Convert curl to Python:
>
> https://curlconverter.com/#python
>
> https://sqqihao.github.io/trillworks.html -> Works better when 2 params

## Fields

```shell
$ curl -i -H "Authorization: token ghp_azertyuiop123456789" \
	https://api.github.com/repos/mysqljs/mysql/comments?per_page=1
```

:arrow_down:

```json
HTTP/2 200 
server: GitHub.com
date: Mon, 06 Jun 2022 09:35:26 GMT
content-type: application/json; charset=utf-8
content-length: 2312
cache-control: private, max-age=60, s-maxage=60
vary: Accept, Authorization, Cookie, X-GitHub-OTP
etag: "e01a6e98230784515c04e381b6c2aa198527ea6328bd1d16158bf1e79f885a6e"
x-oauth-scopes: repo, user, write:discussion
x-accepted-oauth-scopes: 
github-authentication-token-expiration: 2022-08-30 07:35:46 UTC
x-github-media-type: github.v3; format=json
link: <https://api.github.com/repositories/805461/comments?per_page=1&page=2>; rel="next", <https://api.github.com/repositories/805461/comments?per_page=1&page=112>; rel="last"
x-ratelimit-limit: 5000
x-ratelimit-remaining: 4998
x-ratelimit-reset: 1654511699
x-ratelimit-used: 2
x-ratelimit-resource: core
access-control-expose-headers: ETag, Link, Location, Retry-After, X-GitHub-OTP, X-RateLimit-Limit, X-RateLimit-Remaining, X-RateLimit-Used, X-RateLimit-Resource, X-RateLimit-Reset, X-OAuth-Scopes, X-Accepted-OAuth-Scopes, X-Poll-Interval, X-GitHub-Media-Type, X-GitHub-SSO, X-GitHub-Request-Id, Deprecation, Sunset
access-control-allow-origin: *
strict-transport-security: max-age=31536000; includeSubdomains; preload
x-frame-options: deny
x-content-type-options: nosniff
x-xss-protection: 0
referrer-policy: origin-when-cross-origin, strict-origin-when-cross-origin
content-security-policy: default-src 'none'
vary: Accept-Encoding, Accept, X-Requested-With
x-github-request-id: F7DF:111B2:119EC45:120AB81:629DCA5E

[
  {
    "url": "https://api.github.com/repos/mysqljs/mysql/comments/133136",
    "html_url": "https://github.com/mysqljs/mysql/commit/09df6f99dfa7e874a89ecf0115a2c40890d64f8c#commitcomment-133136",
    "id": 133136,
    "node_id": "MDEzOkNvbW1pdENvbW1lbnQxMzMxMzY=",
    "user": {
      "login": "piscisaureus",
      "id": 218257,
      "node_id": "MDQ6VXNlcjIxODI1Nw==",
      "avatar_url": "https://avatars.githubusercontent.com/u/218257?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/piscisaureus",
      "html_url": "https://github.com/piscisaureus",
      "followers_url": "https://api.github.com/users/piscisaureus/followers",
      "following_url": "https://api.github.com/users/piscisaureus/following{/other_user}",
      "gists_url": "https://api.github.com/users/piscisaureus/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/piscisaureus/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/piscisaureus/subscriptions",
      "organizations_url": "https://api.github.com/users/piscisaureus/orgs",
      "repos_url": "https://api.github.com/users/piscisaureus/repos",
      "events_url": "https://api.github.com/users/piscisaureus/events{/privacy}",
      "received_events_url": "https://api.github.com/users/piscisaureus/received_events",
      "type": "User",
      "site_admin": false
    },
    "position": null,
    "line": null,
    "path": null,
    "commit_id": "09df6f99dfa7e874a89ecf0115a2c40890d64f8c",
    "created_at": "2010-08-23T09:54:19Z",
    "updated_at": "2010-08-23T09:54:19Z",
    "author_association": "NONE",
    "body": "This doesn't handle null values; now there is no distinction between null and the empty string. Try:\n\nclient.query('SELECT NULL as field_a, NULL as field_b, \"\" AS field_c', function(err, results) {\n  if (err) throw err;\n\n  assert.strictEqual(results[0].field_a, null);\n  assert.strictEqual(results[0].field_b, null);\n  assert.strictEqual(results[0].field_c, '');\n  client.end();\n});\n",
    "reactions": {
      "url": "https://api.github.com/repos/mysqljs/mysql/comments/133136/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    }
  }
]
```

**Interesting fields**: 

- Header
  - `HTTP/2 200` (first line): Status of the response
  - `x-ratelimit-remaining: 4998`: Number of remaining queries for the hour
  - `github-authentication-token-expiration`: Limit date of the token
- Body
  - `URL`?: URL of the comment (Probably useless)
  - `body`: The comment

:arrow_right: Header: Display it and warn for the token - Stop if bad status

:arrow_right: Body: CSV or JSON with `label` - `text`
