# Focus on MongoDB and MySQL

There are 2 ways to play with the GitHub API:

- With command lines, with `curl`
  - The `-H "Authorization: token ghp_azertyuiop123456789"` is not mandatory I think
- Directly by passing the URL in a browser

Documentation:

- https://docs.github.com/en/rest/guides/getting-started-with-the-rest-api

## General

- Get all the repos with "MongoDB" (default: 30)

```shell
$ curl -i -H "Authorization: token ghp_azertyuiop123456789" \
	https://api.github.com/search/repositories?q=mongodb
```

https://api.github.com/search/repositories?q=mongodb

- Get 100 repos with "MySQL"

```shell
$ curl -i -H "Authorization: token ghp_azertyuiop123456789" \
	https://api.github.com/search/repositories?q=mysql&per_page=100
```

https://api.github.com/search/repositories?q=mysql&per_page=100

## Zoom on comments for one repo

- Get 10 comments for the repo "mysqljs/mysql"

```shell
$ curl -i -H "Authorization: token ghp_azertyuiop123456789" \
	https://api.github.com/repos/mysqljs/mysql/comments?per_page=10
```

https://api.github.com/repos/mysqljs/mysql/comments?per_page=10

:arrow_right: `repos/x/y/comments`: x & y come from the previous request (`items/nb/full_name`)