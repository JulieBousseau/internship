# TODO

1. Looking at Deep Learning Transformers from here 
   

https://huggingface.co/
https://huggingface.co/docs/transformers/task_summary

2. This is a visual understanding of transformers 

https://jalammar.github.io/illustrated-transformer/

3. Once you are familiar with 1) and 2) we should fine-tune a transformer on some data of our own

https://huggingface.co/docs/transformers/training

# Links

- Sentiment analysis with Transformers

https://huggingface.co/blog/sentiment-analysis-python#:~:text=Sentiment%20analysis%20is%20the%20automated,detect%20insights%20and%20automate%20processes.

- Other links:

https://huggingface.co/docs/transformers/pipeline_tutorial
https://huggingface.co/docs/transformers/installation

# Quick look

## Installation

```shell
# Create a virtual environment
python -m venv .env
source .env/bin/activate

# Install libraries
pip install transformers
pip install TensorFlow
```

## Test

```python
from transformers import pipeline

classifier = pipeline("sentiment-analysis")

result = classifier("The sun is shining!")[0]
print(f"label: {result['label']}, with score: {round(result['score'], 4)}")
```

```shell
& python3 test.py
> label: POSITIVE, with score: 0.9999
```

## Fine-Tune 

### Dataset

- JSON:

```python
from datasets import load_dataset
dataset = load_dataset('json', data_files='my_file.json')
```

Ex. formats:

```json
{"a": 1, "b": 2.0, "c": "foo", "d": false}
{"a": 4, "b": -5.5, "c": null, "d": true}
```


```json
{"version": "0.1.0",
    "data": [{"a": 1, "b": 2.0, "c": "foo", "d": false},
            {"a": 4, "b": -5.5, "c": null, "d": true}]
}
```
:arrow_down:

```python
from datasets import load_dataset
dataset = load_dataset('json', data_files='my_file.json', field='data')
```
- CSV:

```python
from datasets import load_dataset
dataset = load_dataset('csv', data_files='my_file.csv')
```

We can also want to separate the training data from the test file:

```python
dataset = load_dataset('csv', data_files={'train': ['my_train_file_1.csv', 'my_train_file_2.csv'], 'test': 'my_test_file.csv'})
```

- Documentation: https://huggingface.co/docs/datasets/loading

## Tokenization

```python
from transformers import AutoTokenizer

tokenizer = AutoTokenizer.from_pretrained("bert-base-cased")


def tokenize_function(examples):
    return tokenizer(examples["text"], padding="max_length", truncation=True)


tokenized_datasets = dataset.map(tokenize_function, batched=True)
```

## Train

- Ex.: https://github.com/huggingface/transformers/tree/main/examples

- Documentation: https://huggingface.co/docs/transformers/training