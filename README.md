# Sentiment analysis of GitHub comments

<!-- TODO UPDATE README !-->

## Description

The motivation of this project is to analyze the emotions and sentiments expressed by MongoDB users. 

To do so, there are differents steps:
- Retrieve users' thougts on the MongoDB technology
  - GitHub comments are used because there are a good place for developers to give their thougts on a technology
- Analyze the emotions of these comments to find if they are rather positive or negative
  - With Transformers by HuggingFace
  - With Sentistrenght
- Compare both analysis and keep the best classifier

This project is built as part of my internship for the University of Rennes 1 (France) and the University of Cagliary (Italy).

## Installation

The project is built in Python. So you'll first need to install Python:

```bash
sudo apt install python3
```

Moreover, you'll need to install the following libraries (if they're not):

```bash
pip install termcolor
pip install argparse
pip install requests
pip install pandas
pip install transformers
pip install tensorflow
```

## Usage

Once in the `src` folder (`cd src`), there are several scripts to run.

### Getting the comments

As mentioned in the **Description** part, the first thing needed is retrieving the number of wanted comments in the interesting repositories.  This is the aim of the script `github_comments.py` (in `src/github_comments/`).

> **Notes**:
>
> - The script uses the GitHub API. You'll need a GitHub token: see [Set a token on GitHub](https://github.com/settings/tokens).
> - You can have less comments than what you asked for if a repo doesn't have enough comments.

:book: **Help**

```bash
python3 github_comments/github_comments.py --help
```

:grey_question: **Syntax**

```bash
python3 github_comments/github_comments.py --github_token <GITHUB_TOKEN> --nb_repos <NB_REPOS> --nb_coms <NB_COMS> --repos_name <REPOS_NAME> [--append yes] [--help]
```

:point_right: **Example**

```bash
python3 github_comments/github_comments.py --github_token ghp_azertyuiop123456789 --nb_repos 5 --nb_coms 100 --repos_name mongodb
```
- Here, you'll get up to 100 comments relative to MongoDB, in 5 repos (20 comments per repo)

This script will write in a CSV file (by default, `data/comments_cleaned_empty.csv`) all the comments, and create the columns for the next steps.

### Analyzing their sentiments

The second step is to analyze the emotion of each comment. It is done by two sentiment analysis classifiers: [Transformers by HuggingFace](https://huggingface.co/docs/transformers/index) and [Sentistrenght](http://sentistrength.wlv.ac.uk/). This is the aim of the script `sentiment_analysis.py` (in `src/sentiment_analysis/`).

At the beginning, comments have to be cleaned: code blocks, links and answers/citations, because they can give a bad classification to the original text.

This script will add in a CSV file (by default, `data/comments_cleaned_transformers.csv`) all the comments and their classification according to Transformers.


:point_right: **Example**

```bash
python3 sentiment_analysis/sentiment_analysis.py 
```

### Practical example

In the reality, we can want to retrieve 100 comments: 50 from MongoDB repos, and 50 for MySQL repos. We can do it by launching the following commands:

```bash
python3 github_comments/github_comments.py --github_token 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 --nb_repos 1 --nb_coms 50 --repos_name mongodb

python3 github_comments/github_comments.py --github_token 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 --nb_repos 1 --nb_coms 50 --repos_name mysql --append yes

python3 sentiment_analysis/sentiment_analysis.py 
```

In the second command, we use the  `--append yes` to add the data at the end of the previous file instead of replacing it.

## Final output

After the Transformers analysis, we obtain a CSV file (`data/comments_cleaned_transformers.csv`) with the following fields:

comment,removed_code,label_transformers,confidence_transformers,label_laurent,label_diego,label_julie

- `comment`: the cleaned comment (i.e. without code blocks, links and citations/answers)
- `removed_code`: if a code block has been removed
- `label_transformers`: the analysis by Transformers
- `confidence_transformers`: the confidence score from the classifier
- `label_laurent`: *(empty)* the label associated by Laurent 
- `label_diego`: *(empty)* the label associated by Diego 
- `label_julie`: *(empty)* the label associated by Julie 

<!-- TODO HERE ADD OTHER SCRIPTS !-->

## Usage of the scripts for step 1

The first step is comparing Transformers and Sentistrength on a set of 100 comments (50 MongoDB, 50 MySQL) to keep the best classifier.

The steps are the following:

```bash
python3 github_comments/github_comments.py --github_token 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 --nb_repos 1 --nb_coms 50 --repos_name mongodb
python3 github_comments/github_comments.py --github_token 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 --nb_repos 1 --nb_coms 50 --repos_name mysql --append yes
# Returns comments_cleaned_empty.csv

python3 sentiment_analysis/analysis_transformers.py 
# Returns comments_cleaned_transformers.csv

python3 sentiment_analysis/analysis_sentistrength.py
# Returns comments_cleaned_sentistrength.csv

python3 sentiment_analysis/merge_classifications.py
# Returns comments_cleaned_full.csv

python3 tools/annotation_file.py
# Returns comments_cleaned_annotated.csv
```