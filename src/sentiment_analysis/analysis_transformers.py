'''Setup environment
pip install termcolor
pip install argparse
'''

'''Ex.:
python3 sentiment_analysis.py --topic mongodb
'''

# Import packages
from modules import clean_comments;
from modules import clean_csv;
from termcolor import colored
from transformers import AutoTokenizer, AutoModelForSequenceClassification
from transformers import pipeline
import pandas as pd
import argparse
from os.path import exists

# ---------- Functions ----------

def handle_parameters() -> object:
    """Function to handle the parameters given in the command line

    Returns:
        args(object): structure with the arguments
    """    
    # -- Create the parser
    parser = argparse.ArgumentParser();

    # -- Add the wanted arguments
    parser.add_argument('--topic', type=str, required=True, help='Name of the topic (--repos_name from github_comments)');

    # -- Parse them
    args = parser.parse_args();

    return args;

# ---------- Main ----------

# -- Setup the files path
args = handle_parameters();

file_path = f'../data/{args.topic}_empty.csv';
file_path_cleaned = f'../data/{args.topic}_cleaned_empty.csv';
file_new_path = f'../data/{args.topic}_transformers.csv';
file_new_path_cleaned = f'../data/{args.topic}_cleaned_transformers.csv';

if exists(file_path):
    # -- Clean the comments
    clean_comments.main(args.topic);
    clean_csv.main(file_path, file_path_cleaned);

# -- Setup the sentiment analysis model
model_name = "j-hartmann/sentiment-roberta-large-english-3-classes";
model = AutoModelForSequenceClassification.from_pretrained(model_name);
tokenizer = AutoTokenizer.from_pretrained(model_name, model_max_len=512);


model_pipeline = pipeline("sentiment-analysis",model=model,tokenizer=tokenizer);
tokenizer_kwargs = {'padding':True,'truncation':True,'max_length':512}


# classifier = pipeline("sentiment-analysis", model=model, tokenizer=tokenizer);

# -- Open the file with the comments
file = pd.read_csv(file_path_cleaned);
comments = file['comment'];

list_scores_transformers = list();
i = 1;

# -- Launch the module to analyze the sentiments with Transformers
for comment in comments:
    print(colored(f'Comment {i}', 'yellow'));
    print(f'>>> {comment}');

    if not pd.isna(comment):
        # res = classifier(comment);
        res = model_pipeline(comment,**tokenizer_kwargs)
        print(res);
        list_scores_transformers.append(res);
    else:
        print('empty comment');
        list_scores_transformers.append('empty');

    i+=1;

# -- Write the comments and the labels in the new file
i = 0;

for score_transformers in list_scores_transformers:
    # The structure is particular (ex.: [{'label': 'NEGATIVE', 'score': 0.9681645035743713}]), so we have to get rid of the "container" object
    if score_transformers != 'empty':
        for truc in score_transformers: 
            label = truc['label'];
            confidence = truc['score'];

            file['label_transformers'][i] = label;
            file['confidence_transformers'][i] = confidence;

            i+=1;
    else:
        file['label_transformers'][i] = 'neutral';
        file['confidence_transformers'][i] = '1';

# -- Write the new fields in the file
file.to_csv(file_new_path);

clean_csv.main(file_new_path, file_new_path_cleaned);

        