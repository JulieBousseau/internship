# Import packages
from unittest import main
from transformers import pipeline
from transformers import AutoTokenizer, AutoModelForSequenceClassification


# ---------- Functions definition ----------
def analyze_sentiment(comment):
    '''
        Pipeline allows us to use a pretrained model very easily.
        It does pre-processing, model and post-processing.
    '''

    # -- Choice of the classifier
    model_name = "j-hartmann/sentiment-roberta-large-english-3-classes";
    model = AutoModelForSequenceClassification.from_pretrained(model_name);
    tokenizer = AutoTokenizer.from_pretrained(model_name);

    senti_classifier = pipeline("sentiment-analysis", model=model, tokenizer=tokenizer);

    # -- The comment we want to classify
    result = senti_classifier(comment[0]);

    # -- Print of the results
    # print(f'{result');

    return result;


# ---------- Main ----------

def main(comment):
    """Main function to call"""    
    result = analyze_sentiment(comment);

    return result;

if __name__ == "__main__":
    main(comment);
