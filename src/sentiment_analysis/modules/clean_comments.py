import re
import pandas as pd
import csv


# ---------- Functions definition ----------

def remove_links(text):
    """Function that removes links from a text
    Links start with http:// or https://

    Args:
        text (str): text to clean

    Returns:
        text (str): text without the links
    """

    text = re.sub('http://.*', '', text);
    text = re.sub('https://.*', '', text);

    return text;

def remove_answers(text):
    """Function that removes answers and citations from a text

    Args:
        text (str): text to clean

    Returns:
        text (str): text without the code blocks
    """

    text = re.sub('>.*', '', text); # Answers and citations lines start with > 
    text = re.sub('On [a-zA-Z]{3}.*', '', text); # The first line of an answer start with the date
    text = re.sub('reply@.*', '', text); # The second line is an email address

    return text;

def remove_code(text):
    """Function that removes code blocks from a text
    They can have 3 forms: <pre>...</pre>, <code>...</code> and ```...```
    This function can't remove the non-formatted code

    Args:
        text (str): text to clean

    Returns:
        text (str): text without the code blocks
    """

    res = re.subn('<pre>.*?</pre>', '', text, flags=re.DOTALL);
    text = res[0];
    flag = res[1];

    res = re.subn('<code>.*?</code>', '', text, flags=re.DOTALL);
    text = res[0];
    flag += res[1];

    res = re.subn('```.*?```', '', text, flags=re.DOTALL);
    text = res[0];
    flag += res[1];

    final_res = [text, flag];
    return final_res;

def clean_comment(comment):
    """Function that cleans a comment, removing links, answers, code blocks and \n

    Args:
        comment (str): comment to clean

    Returns:
        comment (str): cleaned comment
    """

    # -- Remove links, code and answers/citations
    comment = remove_links(comment);

    res = remove_code(comment);
    comment = res[0];
    flag = res[1];

    comment = remove_answers(comment); 

    # -- Remove empty lines and \n
    comment = "\n".join([ll.rstrip() for ll in comment.splitlines() if ll.strip()]);
    comment = comment.replace('\n', '');

    return [comment, flag];

# ---------- Main ----------

def main(topic):
    """Main function to call"""

    # -- Setup the files path (a new file is created)
    file_path = f'../data/{topic}_empty.csv'

    # -- Open the file with the comments
    file = pd.read_csv(file_path);

    # -- Browse each comment and clean it
    for i in file.index:
        comment = file['comment'][i];

        cleaned_res = clean_comment(comment);
        cleaned_comment = cleaned_res[0];
        flag = cleaned_res[1];
        
        if flag > 0:
            file['removed_code'][i] = 'yes';
        else:
            file['removed_code'][i] = 'no';

        file['comment'][i] = cleaned_comment;
    
    file.to_csv(file_path);

if __name__ == "__main__":
    main(topic);