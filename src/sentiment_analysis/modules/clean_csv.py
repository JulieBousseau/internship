import csv
import os


# ---------- Main ----------

def main(file_path, file_new_path):
    """Main function to call"""

    with open(file_path, "r") as source: 
            reader = csv.reader(source);
            
            with open(file_new_path, "w") as result: 
                writer = csv.writer(result);
                for r in reader: 
                    writer.writerow((r[1], r[2], r[3], r[4], r[5]));

    os.remove(file_path);

if __name__ == "__main__":
    main(file_path, file_new_path);