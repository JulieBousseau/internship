from sentistrength import PySentiStr
import pandas as pd
from modules import clean_csv;


file_path = '../data/comments_cleaned_empty.csv';
file_new_path = '../data/comments_sentistrength.csv';
file_new_path_cleaned = '../data/comments_cleaned_sentistrength.csv'

senti = PySentiStr();
senti.setSentiStrengthPath('/home/knayz/Documents/internship/src/sentiment_analysis/SentiStrengthCom.jar');
senti.setSentiStrengthLanguageFolderPath('/home/knayz/Documents/internship/src/sentiment_analysis/SentiStrengthDataEnglishOctober2019');

file = pd.read_csv(file_path);
comments = file['comment'].to_list();

list_classifications_sentistrength = senti.getSentiment(comments, score='scale');

i = 0;

for classification_sentistrength in list_classifications_sentistrength:
    file['label_sentistrength'][i] = classification_sentistrength;

    i+=1;

# -- Write the new fields in the file
file.to_csv(file_new_path);

clean_csv.main(file_new_path, file_new_path_cleaned);
