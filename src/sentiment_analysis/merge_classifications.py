'''Setup environment
pip install pandas
'''

'''Ex.:
python3 merge_classifications.py
'''

# Import packages
from ast import arg
import pandas as pd
import csv
import argparse

def handle_parameters() -> object:
    """Function to handle the parameters given in the command line

    Returns:
        args(object): structure with the arguments
    """    
    # -- Create the parser
    parser = argparse.ArgumentParser();

    # -- Add the wanted arguments
    parser.add_argument('--labels', type=str, required=True, help='\'yes\' if there is a label file to merge');

    # -- Parse them
    args = parser.parse_args();

    return args;

# ---------- Main ----------
args = handle_parameters();

file_path_transformers = '../data/comments_cleaned_transformers.csv';
file_path_sentistrength = '../data/comments_cleaned_sentistrength.csv';
file_path_full = '../data/comments_cleaned_full.csv';

file_transformers = pd.read_csv(file_path_transformers);
file_sentistrength = pd.read_csv(file_path_sentistrength);

if args.labels == 'yes':
    file_labels = pd.read_csv(file_path_labels);
    file_path_labels = '../data/comments_cleaned_labels.csv';

for i in file_transformers.index:
    comment = file_transformers['comment'][i];
    removed_code = file_transformers['removed_code'][i];
    label_transformers = file_transformers['label_transformers'][i];
    confidence_transformers = file_transformers['confidence_transformers'][i];

    if args.labels == 'yes':
        label_laurent = file_labels['label_laurent'][i];
        label_diego = file_labels['label_diego'][i];
        label_julie = file_labels['label_julie'][i];
    else:
        label_laurent = '-';
        label_diego = '-';
        label_julie = '-';

    label_sentistrength = file_sentistrength['label_sentistrength'][i];

    with open(file_path_full, "a") as file_full: 
        writer_full = csv.writer(file_full);

        if i == 0:
            writer_full.writerow(['comment', 'removed_code', 'label_transformers', 'confidence_transformers', 'label_sentistrength', 'label_laurent', 'label_diego', 'label_julie'])
        
        writer_full.writerow((comment, removed_code, label_transformers, confidence_transformers, label_sentistrength, label_laurent, label_diego, label_julie));
