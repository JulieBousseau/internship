#!/bin/bash

listSystems="oracledb mysql postgresql mongodb redis ibmdb elasticsearch sqlite"

for system in $listSystems; do
    python3 sentiment_analysis/analysis_transformers.py --topic $system
done
