'''Setup environment
pip install termcolor
pip install argparse
'''

'''Ex.:
python3 sentiment_analysis.py --file_path ../../data/comments_empty.csv
'''

# Import packages
import pandas as pd
import argparse


# ---------- Functions definition ----------

def handle_parameters() -> object:
    """Function to handle the parameters given in the command line

    Returns:
        args(object): structure with the arguments
    """    
    # -- Create the parser
    parser = argparse.ArgumentParser();

    # -- Add the wanted arguments
    parser.add_argument('--file_path', type=str, required=True, help='Path of the CSV file with the comments');

    # -- Parse them
    args = parser.parse_args();

    return args;


# ---------- Main ----------
def main():
    args = handle_parameters();

    file = pd.read_csv(args.file_path);

    print(file);
    print('.......');

    # print(file['comment'][0]);
    # file['comment'][0] = "TEST ENCORE";
    # print(file['comment'][0]);

    file['label_transformers'][0] = "labelle";
    print(file);

    file.to_csv('../data/comments_transformers.csv');
