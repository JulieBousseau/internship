import glob
import os
import csv
import re

# --- 1. Create 2 lists (one with the years, one with the sentiments) for each system ---

# Get all the files names (Transformers and dates)
list_names_transformers = list();
for root, dirs, files in os.walk('../data/'):
    for name in files:
        if name.endswith(("_transformers.csv")):
            list_names_transformers.append(name);

list_names_dates = list()
for root, dirs, files in os.walk('../data/dates'):
    for name in files:
        if name.endswith(("empty.csv")):
            list_names_dates.append(name);

list_names_transformers.sort();
list_names_dates.sort();

pattern = '^[0-9]{4}';
nb_file = 0;

list_sentiments = list();
list_years = list();

# Browse every file (every system)
for name_file in list_names_transformers:
    full_transformers = f'../data/{name_file}';
    full_dates = f'../data/dates/{list_names_dates[nb_file]}';
    full_tmp = f'../data/charts/{name_file}_tmp_year_sentiment.csv';
    
    # Create the lists thanks to the files (Transformers and Dates)
    with open(full_transformers, 'r') as file_transformers:
        reader = csv.reader(file_transformers);

        line_transformers = 0;
        for row in reader:
            if line_transformers != 0:
                sentiment = row[2];
                list_sentiments.append(sentiment);

                system = row[4];

            line_transformers += 1;

    with open(full_dates, 'r') as file_dates:
        reader = csv.reader(file_dates);

        line_dates = 0;
        for row in reader:
            if line_dates != 0:
                ugly_date = row[1];
                year = int(re.search(pattern=pattern, string=ugly_date).group(0));
                list_years.append(year);

            line_dates += 1;

    # --- 2. Count the number of negative/neutral/positive for each year ---
    
    list_unique_years = list((set(list_years)));
    list_unique_years.sort();

    list_total_senti = list();
    dict_year_senti = dict();

    for unique_year in list_unique_years:
        count_negative = 0;
        count_neutral = 0;
        count_positive = 0;

        i = 0;

        for year in list_years:
            if year == unique_year:
                sentiment = list_sentiments[i];

                if sentiment == 'negative':
                    count_negative += 1;
                elif sentiment == 'neutral':
                    count_neutral += 1;
                else:
                    count_positive += 1;

            i += 1;

        list_total_senti = [count_negative, count_neutral, count_positive];
        dict_year_senti[unique_year] = list_total_senti;


        list_total_senti = [];
        count_negative = 0;
        count_neutral = 0;
        count_positive = 0;

    # --- 3. Write them in a csv ---
    
    # Create a list with all the years since the creation of GitHub (first comment)
    list_all_years = list();
    for y in range(2008, 2022 + 1):
        list_all_years.append(y);

    file_path_final = f'../data/charts/{system}_years_sentiments.csv';

    with open(file_path_final, 'w') as file_destination:
        writer = csv.writer(file_destination);

        header = ['year', 'negative', 'neutral', 'positive'];
        writer = csv.DictWriter(file_destination, fieldnames=header);

        writer.writeheader();

        for year_github in list_all_years:
            if year_github not in dict_year_senti:
                writer.writerow({'year': year_github, 'negative': 0, 'neutral': 0, 'positive': 0});
            else:
                negative = dict_year_senti[year_github][0];
                neutral = dict_year_senti[year_github][1];
                positive = dict_year_senti[year_github][2];

                writer.writerow({'year': year_github, 'negative': negative, 'neutral': neutral, 'positive': positive});


    list_sentiments = [];
    list_years = [];
    nb_file += 1;

# Delete the tmp files
list_files_delete = glob.glob('../data/charts/*_tmp_year_sentiment.csv');
for file_delete in list_files_delete:
    try:
        os.remove(file_delete);
    except:
        print("Error while deleting file: ", file_delete);