import pandas as pd

file_path = '../data/comments_cleaned_final.csv';
file_new_path = '../data/comments_cleaned_annotated2.csv';

file = pd.read_csv(file_path);

list_majorities = [];
list_diego_laurent = [];
list_laurent_julie = [];
list_julie_diego = [];
list_real_perf_transformers = [];
list_real_perf_sentistrength = [];

for i in file.index:
    label_diego = file['label_diego'][i];
    label_laurent = file['label_laurent'][i];
    label_julie = file['label_julie'][i];

    label_transformers = file['label_transformers'][i];
    score_sentistrength = file['label_sentistrength'][i];

    if int(score_sentistrength) < -1:
        label_sentistrength = 'negative';
    elif int(score_sentistrength) > 1:
        label_sentistrength = 'positive';
    else:
        label_sentistrength = 'neutral';


    if label_diego != label_laurent and label_diego != label_julie and label_julie != label_laurent:
        majority = 'x';

    elif label_diego == label_laurent:
        majority = label_diego;
    
    else:
        majority = label_julie;

    list_majorities.append(majority);


    if label_diego == label_laurent:
        list_diego_laurent.append('1');
    else:
        list_diego_laurent.append('0');
    
    if label_laurent == label_julie:
        list_laurent_julie.append('1');
    else:
        list_laurent_julie.append('0');

    if label_julie == label_diego:
        list_julie_diego.append('1');
    else:
        list_julie_diego.append('0');

    if label_transformers == majority:
        list_real_perf_transformers.append('1');
    else:
         list_real_perf_transformers.append('0');   
    
    if label_sentistrength == majority:
        list_real_perf_sentistrength.append('1');
    else:
        list_real_perf_sentistrength.append('0');
    

file['majority_voting'] = list_majorities;
file['diego/laurent'] = list_diego_laurent;
file['laurent/julie'] = list_laurent_julie;
file['julie/diego'] = list_julie_diego;
file['real_perf_transformers'] = list_real_perf_transformers;
file['real_perf_sentistrength'] = list_real_perf_sentistrength;

file.to_csv(file_new_path);    

