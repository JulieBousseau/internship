import csv
import os

def write_sentiments(count_negative, count_neutral, count_positive, option='a'):
    with open(file_path_final, option) as file_destination:
        writer = csv.writer(file_destination);

        header = ['system', 'total_comments', 'negative', 'neutral', 'positive'];
        writer = csv.DictWriter(file_destination, fieldnames=header);

        if option == 'w':
            writer.writeheader();

        writer.writerow({'system': system, 'total_comments': total, 'negative': count_negative, 'neutral': count_neutral, 'positive': count_positive});


file_path_final = '../data/charts/system_sentiments.csv';

list_files = list();
for root, dirs, files in os.walk('../data/'):
    for name in files:
        if name.endswith(("_transformers.csv")):
            list_files.append(name);

# Do the process for each date file
nb_file = 1;
for file in list_files:
    count_negative = 0;
    count_neutral = 0;
    count_positive = 0;
    total = -1; # To delete the header

    file = '../data/' + file;

    with open(file, 'r') as file_source:
        reader = csv.reader(file_source);

        for row in reader:
            if row[2] == 'negative':
                count_negative += 1;
            elif row[2] == 'neutral':
                count_neutral += 1;
            elif row[2] == 'positive':
                count_positive += 1;

            total += 1;
            system = row[4];

    if nb_file == 1:
        write_sentiments(count_negative, count_neutral, count_positive, option='w');
    else:
        write_sentiments(count_negative, count_neutral, count_positive);

    nb_file += 1;

# CSV: system, total, negative, neutral, positive
        