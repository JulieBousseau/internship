'''Setup environment
pip install requests
pip install termcolor
pip install argparse
'''

'''Ex.:
python3 github_comments.py --github_token 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 --nb_repos 3 --nb_coms 3 --repos_name mongodb
'''

# Import packages
from modules import get_repos_name;
from modules import get_comments;
from termcolor import colored
import argparse
import csv


# ---------- Functions definition ----------

def handle_parameters() -> object:
    """Function to handle the parameters given in the command line

    Returns:
        args(object): structure with the arguments
    """    
    # -- Create the parser
    parser = argparse.ArgumentParser();

    # -- Add the wanted arguments
    parser.add_argument('--github_token', type=str, required=True, help='GitHub Token');
    parser.add_argument('--nb_repos', type=int, required=True, help='Number of wanted repos'); 
    parser.add_argument('--nb_coms', type=int, required=True, help='Number of wanted coms');
    parser.add_argument('--repos_name', type=str, required=True, help='Repos to look for');
    parser.add_argument('--append', type=str, required=False, help='\'yes\' to add the comments at the end of the comments_empty.csv file instead of replacing it ');

    # -- Parse them
    args = parser.parse_args();

    return args;

def get_list_comments(github_token: str, nb_coms_per_repo: int) -> list:
    """Function to get the list of all the wanted comments
    Note: Supposing we are asking 10 repos and 100 comments. We'll have 10 comments per repo. If a repo doesn't have 10 comments, then there will be less than 100 comments in total.

    Args:
        github_token (str): user token (Generate it from the GitHub website)
        nb_coms_per_repo (int): number of wanted comments per repo

    Returns:
        list_comments (list): list with the retrieved comments
    """    

    list_comments = list();

    for repo in list_repos_name:
        list_got_comments = get_comments.main(github_token, nb_coms_per_repo, repo);
        
        for got_comment in list_got_comments:
            list_comments.append(got_comment);

        list_got_comments.clear();

    return list_comments;

def write_comments_in_csv(option: str):
    """Function to write all the comments into the csv file
    Or to append them at the end of an existing one
    csv path: ../../data/comments_empty.csv
    csv architecture: 'comment', 'removed_code', 'label_transformers', 'confidence_transformers', 'label_sentistrength', 'label_laurent', 'label_diego', 'label_julie'

    Args:
        option(str): 'a' to append or 'w' to write a new file
    
    """  

    with open('../data/comments_empty.csv', option, encoding='UTF8') as f:
        header = ['comment', 'removed_code', 'label_transformers', 'confidence_transformers', 'label_sentistrength', 'label_laurent', 'label_diego', 'label_julie'];

        writer = csv.DictWriter(f, fieldnames=header);

        if option == 'w':
            writer.writeheader();

        for comment in list_comments:
            writer.writerow({'comment' : comment, 'label_transformers' : '-', 'confidence_transformers' : '-', 'label_sentistrength': '-','label_laurent': '-', 'label_diego': '-', 'label_julie': '-'});


# ---------- Main ----------

args = handle_parameters();

# -- Repos name
print(colored('Step 1: Getting repos name', 'yellow'));

list_repos_name = list();
list_repos_name = get_repos_name.main(args.github_token, args.nb_repos, args.repos_name);

# print(f'>>> List of repos name');
# print(f'{list_repos_name}');

# -- Comments
print(colored('Step 2: Getting comments', 'yellow'));

nb_coms_per_repo = args.nb_coms/args.nb_repos;

list_comments = get_list_comments(args.github_token, nb_coms_per_repo);

# print(f'>>> List of comments');
# print(f'{list_comments}');

# -- Write in a CSV file
print(colored('Step 3: Writing in ../data/comments_empty.csv', 'yellow'));

append_comments = args.append;
if (append_comments == 'yes'):
    write_comments_in_csv('a');
else:
    write_comments_in_csv('w');

# clean_csv.main('../data/comments_empty.csv', '../data/comments_empty.csv');
