'''Setup environment
pip install requests
pip install termcolor
pip install argparse
'''

'''Ex.:
python3 github_comments.py --github_token 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 --nb_coms 3 --repos_name mongodb
'''

# Import packages
from modules import get_repos_name
from modules import get_date
from modules import get_number_comments
from modules import get_number_repos_pages
from termcolor import colored
import argparse
import csv
import os


# ---------- Functions definition ----------

def handle_parameters() -> object:
    """Function to handle the parameters given in the command line

    Returns:
        args(object): structure with the arguments
    """    
    # -- Create the parser
    parser = argparse.ArgumentParser();

    # -- Add the wanted arguments
    parser.add_argument('--github_token', type=str, required=True, help='GitHub Token');
    parser.add_argument('--nb_repos', type=int, required=True, help='Number of wanted repos'); 
    parser.add_argument('--nb_coms', type=int, required=True, help='Number of wanted coms');
    parser.add_argument('--repos_name', type=str, required=True, help='Repos to look for');
    parser.add_argument('--append', type=str, required=False, help='\'yes\' to add the comments at the end of the comments_empty.csv file instead of replacing it ');

    # -- Parse them
    args = parser.parse_args();

    return args;

def create_files():
    file_repos = open('../data/dates/nb_repos', "w");
    file_repos.write(str(0));
    file_repos.close();

    file_pages = open('../data/dates/nb_page', "w");
    file_pages.write(str(1));
    file_pages.close();

    file_pages = open('../data/dates/max_page', "w");
    file_pages.write(str(1));
    file_pages.close();

def get_list_dates(github_token: str, nb_coms: int, number_comments: list, repo: str, nb_repos: int, repos_name: str) -> list:
    """Function to get the list of all the wanted dates

    Args:
        github_token (str): user token (Generate it from the GitHub website)
        nb_coms_per_repo (int): number of wanted comments per repo
        list_number_comments: list with the total count of comments for each repo

    Returns:
        list_comments (list): list with the retrieved comments
    """    

    list_dates = list();

    list_got_dates = get_date.main(github_token, nb_coms, repo, number_comments, nb_repos, repos_name);
    # def main(github_token, nb_coms, repo, total_count, old_nb_repos, repos_name):

    
    for got_date in list_got_dates:
        list_dates.append(got_date);

    list_got_dates.clear();

    return list_dates;

def write_comments_in_csv(option: str, repos_name: str):
    """Function to write all the dates into the csv file
    Or to append them at the end of an existing one
    csv path: ../../data/dates/dates_empty.csv
    csv architecture: 'system', 'date'

    Args:
        option(str): 'a' to append or 'w' to write a new file
    
    """  

    with open(f'../data/dates/dates_empty.csv', option, encoding='UTF8') as f:
        header = ['system', 'date'];

        writer = csv.DictWriter(f, fieldnames=header);

        if option == 'w':
            writer.writeheader();

        for date in list_dates:
            writer.writerow({'system': repos_name, 'date': date});


# ---------- Main ----------

args = handle_parameters();

create_files();

# -- Number of repos pages
print(colored('Step 0: Getting number of repos pages', 'yellow'));
get_number_repos_pages.main(args.github_token, args.nb_repos, args.repos_name);

# -- Repos name
print(colored('Step 1: Getting repos name', 'yellow'));

repo = get_repos_name.main(args.github_token, args.nb_repos, args.repos_name);

print(f'>>> Repo name');
print(f'{repo}');

# -- Number of comments
print(colored('Step 2: Getting number of comments', 'yellow'));

number_comments = get_number_comments.main(args.github_token, repo);

print(f'>>> Number of comments');
print(f'{number_comments}');

# -- Comments
print(colored('Step 3: Getting dates', 'yellow'));

nb_coms = int(args.nb_coms)

list_dates = get_list_dates(args.github_token, nb_coms, number_comments, repo, args.nb_repos, args.repos_name);

print(f'>>> List of dates');
i = 0;
for date in list_dates:
    i+=1;
    # print(f'{i}: {comment}');
print(f'{i} dates');

# -- Write in a CSV file
print(colored('Step 4: Writing in ../data/dates/dates_empty.csv', 'yellow'));

append_comments = args.append;
if (append_comments == 'yes'):
    write_comments_in_csv('a', args.repos_name);
else:
    write_comments_in_csv('w', args.repos_name);

os.remove('../data/dates/nb_repos');
os.remove('../data/dates/nb_page');
os.remove('../data/dates/max_page');