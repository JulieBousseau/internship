from lib2to3.pgen2 import token
from urllib import response
import requests
import argparse
import re
import csv

def handle_parameters() -> object:
    """Function to handle the parameters given in the command line

    Returns:
        args(object): structure with the arguments
    """    
    # -- Create the parser
    parser = argparse.ArgumentParser();

    # -- Add the wanted arguments
    parser.add_argument('--github_token', type=str, required=True, help='GitHub Token');
    parser.add_argument('--nb_coms', type=int, required=True, help='Number of wanted coms');
    parser.add_argument('--topic', type=str, required=True, help='Topics of repos to look for');
    parser.add_argument('--append', type=str, required=False, help='\'yes\' to add the comments at the end of the comments_empty.csv file instead of replacing it ');

    # -- Parse them
    args = parser.parse_args();

    return args;

def handle_api(token, api='search/repositories', page=1, query=None):
    """
        Get all public repositories from GitHub API with 5000 requests per hour.
    """
    headers = {
        'Authorization': 'token ' + token,
        'Accept': 'application/vnd.github.v3+json'
    }

    params = {'per_page': 100, 
    'page': page}

    if query:
        params['q'] = query

    result = requests.get('https://api.github.com/' + api, headers=headers, params=params)

    if result.status_code == 200:
        return result.json();
    else:
        return None

def query_api_nb_repos(token: str, query: str) -> object:
    """Function that queries the GitHub API

    Args:
        token (str): user token (Generate it from the GitHub website)
        query (str): name of the repo to look for

    Returns:
        response (object): response from the query
    """    

    # -- Set the params of the request
    headers = {
        'Authorization': 'token ' + token
    };

    params = {
        'q': query,
        'per_page': 100,
    };

    # -- CURL command
    response = requests.get('https://api.github.com/search/repositories', params=params, headers=headers);

    return response;

def query_api_nb_coms(token: str, query: str) -> object:
    """Function that queries the GitHub API

    Args:
        token (str): user token (Generate it from the GitHub website)
        query (str): name of the repo to count the comment

    Returns:
        response (object): response from the query
    """    

    # -- Set the params of the request
    headers = {
        'Authorization': 'token ' + token
    };

    params = {
        'per_page': 100
    };

    # -- CURL command
    response = requests.head(f'https://api.github.com/repos/{query}/comments', params=params, headers=headers);

    return response;

def get_header(response):
    response_header = response.headers;

    if 'link' in response_header:
        response_header_link = response.headers['link'];
    else: 
        response_header_link = 1;

    return response_header_link;

def get_max_page(response):
    header_link = get_header(response);

    if header_link != 1:
        header_link_prime = header_link.split(',');
        pattern = '&page=([0-9]+)';

        pagee_number = re.search(pattern=pattern, string=header_link_prime[1]).group(1);
    
    else:
        pagee_number = header_link;
    
    return pagee_number;

# tests

args = handle_parameters();

token = args.github_token;
query = args.topic;

# Get the number of pages for the repos
max_pages_repos = int(get_max_page(query_api_nb_repos(token, query)));

list_comments = list();

max_com = False;
nb_page_repo = 1;

while nb_page_repo < max_pages_repos + 1 and not max_com:
# for i in range(1, max_pages_repos + 1):
    list_repos = handle_api(token, query=query, page=nb_page_repo);

    # print(f'nb_page_repo: {nb_page_repo} / max_pages_repos: {max_pages_repos}');

    index_repo = 0;
    while index_repo < len(list_repos['items']) and not max_com:
    # for repo in list_repos['items']:

        lengtt = len(list_repos['items']);

        # print(f'\tindex_repo: {index_repo} / len(list_repos): {lengtt}');

        repo = list_repos['items'][index_repo];

        max_pages_coms = int(get_max_page(query_api_nb_coms(token, repo['full_name'])));

        nb_page_coms = 1;
        while nb_page_coms < max_pages_coms + 1 and not max_com:
        # for j in range (1, max_pages_coms + 1):

            # print(f'\t\tnb_page_coms: {nb_page_coms} / max_pages_coms: {max_pages_coms}');

            comments = handle_api(token, api='repos/' + repo['full_name'] + '/comments', page=nb_page_coms);

            if comments:
                len_com = len(comments);

                # print(f'\t\t\tnb comments {len_com}')
                list_comments.extend(comments)

                len_list_com = len(list_comments)
                # print(f'\t\t\tlen list com: {len_list_com}')
                if len(list_comments) >= args.nb_coms:
                    # print('MAX COM TRUE')
                    max_com = True;

            nb_page_coms += 1;

        index_repo += 1;
    
    nb_page_repo += 1;

print('Total comments:', len(list_comments));

list_comments_body = list();
list_comments_dates = list();
for comment in list_comments:
    list_comments_body.append(comment['body']);
    list_comments_dates.append(comment['created_at']);


file_name_comments = f'../data/{query}_empty.csv';
with open(file_name_comments, 'w') as f:
    header = ['comment', 'removed_code', 'label_transformers', 'confidence_transformers', 'system'];

    writer = csv.DictWriter(f, fieldnames=header);
    writer.writeheader();

    for comment in list_comments_body:
        writer.writerow({'comment' : comment, 'removed_code': '-', 'label_transformers' : '-', 'confidence_transformers' : '-', 'system': query});

file_name_dates = f'../data/dates/dates_{query}_empty.csv';
with open(file_name_dates, 'w') as f:
    header = ['system', 'date'];

    writer = csv.DictWriter(f, fieldnames=header);
    writer.writeheader();

    for date in list_comments_dates:
        writer.writerow({'system' : query, 'date': date});