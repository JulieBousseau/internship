import pandas as pd
import re
import csv
import os

# ---------- Functions definition ----------

def write_date_nb_in_csv(total_comments, system, mode='a'):
    with open(f'data/dates/dates_nb.csv', mode = mode, encoding='UTF8') as f:
        header = list_headers;
        writer = csv.DictWriter(f, fieldnames=header);
        
        if mode == 'w':
            writer.writeheader();

        # list_headers.pop(0);

        dict_full_years = dict();
        i = 0;
        for year in list_headers:
            if i == 0:
                dict_full_years['system'] = system;
            elif i == 1:
                dict_full_years['total_comments'] = total_comments;
            
            else:
                if year in dict_date_nb:
                    dict_full_years[year] = dict_date_nb[year];
                else:
                    dict_full_years[year] = 0;
            
            i += 1;

        # print(dict_full_years);

        writer.writerow(dict_full_years);


# ---------- Main ----------

# Write all the years from the creation of GitHub to today
list_headers = list();
list_headers.append('system');
list_headers.append('total_comments')
for i in range (2008, 2022 + 1):
    list_headers.append(i);
        
# Setup files path
# file_path = '../data/dates/dates_empty.csv';
# file = pd.read_csv(file_path);

list_files = list();
for root, dirs, files in os.walk('data/dates/'):
    for name in files:
        if name.endswith(("_empty.csv")):
            list_files.append(name);

# Do the process for each date file
nb_file = 1;
for file in list_files:
    pd_file = pd.read_csv(f'data/dates/{file}');

    # Get all the years thanks to a regular expression
    list_ugly_dates = pd_file['date'].tolist();

    list_years = list();
    for ugly_date in list_ugly_dates:
        pattern = '^[0-9]{4}';
        year = int(re.search(pattern=pattern, string=ugly_date).group(0));
        list_years.append(year);

    # Find how many comments there are for each year
    list_years.sort();

    dict_date_nb = dict();
    if not len(list_years) == 0:
        list_unique_years = list((set(list_years)));
        list_unique_years.sort();

        for unique_year in list_unique_years:
            dict_date_nb[unique_year] = list_years.count(unique_year);

        system = pd_file['system'][0];
        total_comments = len(pd_file.index);

    else:
        system = re.search('_(.*)_', file).group(1);
        total_comments = 0;

    if nb_file == 1:
        write_date_nb_in_csv(mode='w', system=system, total_comments=total_comments);
    else:
        write_date_nb_in_csv(system=system, total_comments=total_comments);

    nb_file += 1;
    
