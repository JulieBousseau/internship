# Import packages
from unittest import main
import requests
from termcolor import colored
import os.path
import pandas as pd
import csv


# ---------- Functions definition ----------

def query_api(token: str, query: str, nb_repos: str) -> object:
    """Function that queries the GitHub API

    Args:
        token (str): user token (Generate it from the GitHub website)
        query (str): name of the repo to look for
        nb_repos (str): number of wanted repos

    Returns:
        response (object): response from the query
    """    

    file_pages = open('../data/dates/nb_page', "r");
    nb_page = int(file_pages.read());
    file_pages.close();

    # print(f'nb_pages: {nb_page}');

    # -- Set the params of the request
    headers = {
        'Authorization': 'token ' + token
    };

    params = {
        'q': query,
        'per_page': 100,
        'page': nb_page
    };

    # -- CURL command
    response = requests.get('https://api.github.com/search/repositories', params=params, headers=headers);

    return response;

def get_headers(response: object):
    """Function that gets and shows the interesting fields of the header

    Args:
        response (object): response from the query
    """    

    # -- All the header
    response_header = response.headers;

    # -- Only the wanted fields
    response_header_ratelimit = response.headers['x-ratelimit-remaining'];
    response_header_expiration = response.headers['github-authentication-token-expiration'];

    # print('>>> Token');
    # print(f'RateLimit: {response_header_ratelimit}');
    # print(f'Token expiration : {response_header_expiration}');

def get_repo(response_json: object, nb_repos: int) -> list:
    """Function that gets, shows and returns the name of the repo

    Args:
        response_json (object): response of the API in JSON format

    Returns:
        repo (list): name of the new repository
    """

    if nb_repos == 1:
        repo = response_json['items'][0]['full_name'];
    else:
        if nb_repos < len(response_json['items']):
            repo = response_json['items'][nb_repos - 1]['full_name'];
        else:
            repo = 'stop';
            
    print(f'>>> Repo name {nb_repos}: {repo}');

    return repo;

def check_repos():
    file_repos = open('../data/dates/nb_repos', "r");
    nb_repos = int(file_repos.read());
    file_repos.close();

    if nb_repos != 99:
        nb_repos += 1;

    else:
        file_pages = open('../data/dates/nb_page', "r");
        nb_page = int(file_pages.read());
        file_pages.close();
        
        nb_page += 1;

        file_pages = open('../data/dates/nb_page', "w");
        file_pages.write(str(nb_page));
        file_pages.close();

        nb_repos = '0';


    file_repos = open('../data/dates/nb_repos', "w");
    file_repos.write(str(nb_repos));
    file_repos.close();

# ---------- Main ----------

def main(github_token, nb_repos, repos_name):
    """Main function to call"""   

    check_repos();

    response = query_api(github_token, repos_name, nb_repos);
    status = response.status_code;

    if response:
        response_json = response.json();

        get_headers(response);
        repo = get_repo(response_json, nb_repos);

        return repo;

    else:
        print('Failure requesting API: get_repo_name');
        exit(1);

if __name__ == "__main__":
    main(github_token, nb_repos, repos_name);
