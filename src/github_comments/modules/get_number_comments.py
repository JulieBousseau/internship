# Import packages
from unittest import main
import requests
from termcolor import colored
import re



# ---------- Functions definition ----------

def query_api(token: str, query: str) -> object:
    """Function that queries the GitHub API

    Args:
        token (str): user token (Generate it from the GitHub website)
        query (str): name of the repo to count the comment

    Returns:
        response (object): response from the query
    """    

    # -- Set the params of the request
    headers = {
        'Authorization': 'token ' + token
    };

    params = {
        'per_page': 1
    };

    # -- CURL command
    response = requests.head(f'https://api.github.com/repos/{query}/comments', params=params, headers=headers);

    return response;

def get_headers(response: object):
    """Function that gets and shows the interesting fields of the header

    Args:
        response (object): response from the query
    """    

    # -- All the header
    response_header = response.headers;

    # # -- Only the wanted fields
    # response_header_ratelimit = response.headers['x-ratelimit-remaining'];
    # response_header_expiration = response.headers['github-authentication-token-expiration'];

    if 'link' in response_header:
        response_header_link = response.headers['link'];
    else: 
        response_header_link = 1;

    # print('>>> Token');
    # print(f'RateLimit: {response_header_ratelimit}');
    # print(f'Token expiration : {response_header_expiration}');

    return response_header_link;


# ---------- Main ----------

def main(github_token, repos_name):
    """Main function to call"""    

    response = query_api(github_token, repos_name);
    status = response.status_code;

    if response:
        header_link = get_headers(response);

        if header_link != 1:
            header_link_prime = header_link.split(',');
            pattern = '&page=([0-9]+)';

            pagee_number = re.search(pattern=pattern, string=header_link_prime[1]).group(1);
        
        else:
            pagee_number = header_link;
        
        return int(pagee_number);
    
    else:
        print('Failure requesting API: get_number_comments');
        exit(1);

if __name__ == "__main__":
    main(github_token, repos_name);
