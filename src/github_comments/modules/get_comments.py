# Import packages
from unittest import main
import requests
from termcolor import colored


# ---------- Functions definition ----------

def query_api(token: str, query: str, nb_coms: str) -> object:
    """Function that queries the GitHub API

    Args:
        token (str): user token (Generate it from the GitHub website)
        query (str): name of the repo to look for
        nb_coms (str): number of wanted comments

    Returns:
        response (object): response from the query
    """    

    # -- Set the params of the request
    headers = {
        'Authorization': 'token ' + token
    };

    # -- CURL command
    response = requests.get(f'https://api.github.com/repos/{query}/comments?per_page={int(nb_coms)}', headers=headers);
    # https://api.github.com/repos/mysqljs/mysql/comments?per_page=10

    return response;

def get_headers(response: object):
    """Function that gets and shows the interesting fields of the header

    Args:
        response (object): response from the query
    """    

    # -- All the header
    response_header = response.headers;

    # -- Only the wanted fields
    response_header_ratelimit = response.headers['x-ratelimit-remaining'];
    response_header_expiration = response.headers['github-authentication-token-expiration'];

    print('>>> Token');
    print(f'RateLimit: {response_header_ratelimit}');
    print(f'Token expiration : {response_header_expiration}');

def get_comments(response_json: object, nb_coms: int) -> list:
    """Function that gets, shows and returns the comments

    Args:
        response_json (object): response of the API in JSON format

    Returns:
        list_comments (list): list of comments
    """

    list_comments = list();
    list_comments.clear();

    for line_response in response_json:
        comment = line_response['body'];
        list_comments.append(comment)

    return list_comments;


# ---------- Main ----------

def main(github_token, nb_coms, repo):
    """Main function to call"""    

    response = query_api(github_token, repo, nb_coms);
    status = response.status_code;

    if response:
        response_json = response.json();

        get_headers(response);
        list_comments = get_comments(response_json, nb_coms);

        return list_comments;
    else:
        print('Failure requesting API: get_comments');       
        exit(1);

if __name__ == "__main__":
    main(github_token, nb_coms, repo);
