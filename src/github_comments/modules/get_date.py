# Import packages
from unittest import main
from modules import get_repos_name
from modules import get_number_comments
import requests
from termcolor import colored
import re


# ---------- Functions definition ----------

def query_api(token: str, query: str, nb_coms: str, nb_page: int) -> object:
    """Function that queries the GitHub API

    Args:
        token (str): user token (Generate it from the GitHub website)
        query (str): name of the repo to look for
        nb_coms (str): number of wanted comments

    Returns:
        response (object): response from the query
    """    

    # -- Set the params of the request
    headers = {
        'Authorization': 'token ' + token
    };

    params = {
        'page': nb_page,
        'per_page': nb_coms
    };

    # -- CURL command
    response = requests.get(f'https://api.github.com/repos/{query}/comments', headers=headers, params=params);

    # https://api.github.com/repos/mysqljs/mysql/comments?per_page=10

    return response;

def handle_number_comments(token: str, nb_coms: int, repo: str, total_count: str, nb_page: int, old_nb_repos: int, repos_name: str, index: int, list_dates: list):
    if nb_coms < 100 and total_count < 100 and nb_coms < total_count:
        list_dates.extend(handle_api(token, repo, 100, nb_page)[0:nb_coms]);

    else:
        if nb_coms > 100:
            list_dates.extend(handle_api(token, repo, 100, nb_page));

            nb_page += 1;
            nb_coms -= 100;
            index += 100;

            if nb_coms == 0:
                return list_dates;
                
            if index < total_count:
                handle_number_comments(token, nb_coms, repo, total_count, nb_page, old_nb_repos, repos_name, index, list_dates);
            
            else:
                list_dates.extend(handle_api(token, repo, 100, nb_page)[0:total_count - index]);
                nb_coms -= total_count - index;
                index = 0;

                # file_repos = open('../data/dates/nb_repos', "r");
                # nb_repo = int(file_repos.read());
                # file_repos.close();

                # if nb_repo != 99:
                new_nb_repos = old_nb_repos + 1;
                new_repo = get_repos_name.main(token, new_nb_repos, repos_name);

                if new_repo == 'stop':
                    return list_dates;

                new_total_count = get_number_comments.main(token, new_repo);
                new_nb_page = 1;

                handle_number_comments(token, nb_coms, new_repo, new_total_count, new_nb_page, new_nb_repos, repos_name, index, list_dates);
        
        else:
            if nb_coms == 0:
                return list_dates;

            else:
                if nb_coms < total_count:
                    list_dates.extend(handle_api(token, repo, 100, nb_page)[0:nb_coms]);
                else:
                    list_dates.extend(handle_api(token, repo, 100, nb_page)[0:total_count - index]);
                    nb_coms -= total_count - index;
                    index = 0;

                    # file_repos = open('../data/dates/nb_repos', "r");
                    # nb_repo = int(file_repos.read());
                    # file_repos.close();


                    # if nb_repo != 99:
                    new_nb_repos = old_nb_repos + 1;
                    new_repo = get_repos_name.main(token, new_nb_repos, repos_name);

                    if new_repo == 'stop':
                        return list_dates;

                    new_total_count = get_number_comments.main(token, new_repo);
                    new_nb_page = 1;

                    handle_number_comments(token, nb_coms, new_repo, new_total_count, new_nb_page, new_nb_repos, repos_name, index, list_dates);

    return list_dates;
                             
def handle_api(token: str, repo: str, nb_coms: str, nb_page: int) -> list:
    response = query_api(token, repo, nb_coms, nb_page);
    status = response.status_code;


    if response:
        response_json = response.json();

        list_dates = get_dates(response_json);

        return list_dates;

def get_headers(response: object) -> str:
    """Function that gets and shows the interesting fields of the header

    Args:
        response (object): response from the query
    """    

    # -- All the header
    response_header = response.headers;

    # -- Only the wanted fields
    response_header_ratelimit = response.headers['x-ratelimit-remaining'];
    response_header_expiration = response.headers['github-authentication-token-expiration'];

    # print('>>> Token');
    # print(f'RateLimit: {response_header_ratelimit}');
    # print(f'Token expiration : {response_header_expiration}');

def get_dates(response_json: object) -> list:
    """Function that gets, shows and returns the dates

    Args:
        response_json (object): response of the API in JSON format

    Returns:
        list_dates (list): list of dates
    """

    list_dates = list();
    list_dates.clear();

    for line_response in response_json:
        date = line_response['created_at'];
        list_dates.append(date);

    return list_dates;


# ---------- Main ----------

def main(github_token, nb_coms, repo, total_count, old_nb_repos, repos_name):
    """Main function to call"""  

    index = 0;
    list_dates = list();

    file_pages = open('../data/dates/nb_page', "r");
    nb_page = int(file_pages.read());
    file_pages.close();

    file_max_page = open('../data/dates/max_page', "r");
    max_page = int(file_max_page.read());
    file_max_page.close();

    return handle_number_comments(github_token, nb_coms, repo, total_count, nb_page, old_nb_repos, repos_name, index, list_dates);


if __name__ == "__main__":
    main(github_token, nb_coms, repo, total_count, old_nb_repos, repos_name);
