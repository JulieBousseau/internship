# Import packages
from unittest import main
import requests
from termcolor import colored
import os.path
import pandas as pd
import csv
import re


# ---------- Functions definition ----------

def query_api(token: str, query: str, nb_repos: str) -> object:
    """Function that queries the GitHub API

    Args:
        token (str): user token (Generate it from the GitHub website)
        query (str): name of the repo to look for
        nb_repos (str): number of wanted repos

    Returns:
        response (object): response from the query
    """    

    # -- Set the params of the request
    headers = {
        'Authorization': 'token ' + token
    };

    params = {
        'q': query,
        'per_page': 100,
    };

    # -- CURL command
    response = requests.get('https://api.github.com/search/repositories', params=params, headers=headers);

    return response;

def get_headers(response: object):
    """Function that gets and shows the interesting fields of the header

    Args:
        response (object): response from the query
    """    

    # -- All the header
    response_header = response.headers;

    # -- Only the wanted fields
    response_header_ratelimit = response.headers['x-ratelimit-remaining'];
    response_header_expiration = response.headers['github-authentication-token-expiration'];

    # print('>>> Token');
    # print(f'RateLimit: {response_header_ratelimit}');
    # print(f'Token expiration : {response_header_expiration}');

    if 'link' in response_header:
        response_header_link = response.headers['link'];
    else: 
        response_header_link = 1;

    return response_header_link;

def get_repos(response_json: object, nb_repos: int) -> list:
    """Function that gets, shows and returns the names of the repos

    Args:
        response_json (object): response of the API in JSON format

    Returns:
        list_repos_name (list): list of repos name
    """

    list_repos_name = list();

    if nb_repos == 1:
        repo_name = response_json['items'][0]['full_name'];
        list_repos_name.append(repo_name)
    else:
        for i in range (0, int(nb_repos)):
            repo_name = response_json['items'][i]['full_name'];
            list_repos_name.append(repo_name)
            # print(f'>>> Repo name {i}: {repo_name}');

    return list_repos_name;

def check_repos():
    file_repos = open('../data/dates/nb_repos', "r");
    nb_repos = int(file_repos.read());
    file_repos.close();

    if nb_repos != 99:
        nb_repos += 1;

    else:
        file_pages = open('../data/dates/nb_page', "r");
        nb_page = int(file_pages.read());
        file_pages.close();
        
        nb_page += 1;

        file_pages = open('../data/dates/nb_page', "w");
        file_pages.write(str(nb_page));
        file_pages.close();

        nb_repos = '0';


    file_repos = open('../data/dates/nb_repos', "w");
    file_repos.write(str(nb_repos));
    file_repos.close();

def get_max_page(response):
    header_link = get_headers(response);

    if header_link != 1:
        header_link_prime = header_link.split(',');
        pattern = '&page=([0-9]+)';

        pagee_number = re.search(pattern=pattern, string=header_link_prime[1]).group(1);
    
    else:
        pagee_number = header_link;
    
    file_max_page = open('../data/dates/max_page', "w");
    file_max_page.write(pagee_number);
    file_max_page.close();


# ---------- Main ----------

def main(github_token, nb_repos, repos_name):
    """Main function to call"""   

    check_repos();

    response = query_api(github_token, repos_name, nb_repos);
    status = response.status_code;

    if response:
        response_json = response.json();

        get_max_page(response);

        list_repos_name = get_repos(response_json, nb_repos);

        return list_repos_name;

    else:
        print('Failure requesting API: get_repos_name');
        exit(1);

if __name__ == "__main__":
    main(github_token, nb_repos, repos_name);
