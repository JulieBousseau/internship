# Import packages
from unittest import main
from modules import get_repos_name
from modules import get_number_comments
import requests
from termcolor import colored


# ---------- Functions definition ----------

def query_api(token: str, query: str, nb_coms: str, nb_page: int) -> object:
    """Function that queries the GitHub API

    Args:
        token (str): user token (Generate it from the GitHub website)
        query (str): name of the repo to look for
        nb_coms (str): number of wanted comments

    Returns:
        response (object): response from the query
    """    

    # -- Set the params of the request
    headers = {
        'Authorization': 'token ' + token
    };

    params = {
        'page': nb_page,
        'per_page': nb_coms
    };

    # -- CURL command
    response = requests.get(f'https://api.github.com/repos/{query}/comments', headers=headers, params=params);
    # https://api.github.com/repos/mysqljs/mysql/comments?per_page=10

    return response;

def handle_number_comments(token: str, nb_coms: int, repo: str, total_count: str, nb_page: int, old_nb_repos: int, repos_name: str, index: int, list_comments):
    if nb_coms < 100 and total_count < 100 and nb_coms < total_count:
        list_comments.extend(handle_api(token, repo, 100, nb_page)[0:nb_coms]);

    else:
        if nb_coms > 100:
            list_comments.extend(handle_api(token, repo, 100, nb_page));

            nb_page += 1;
            nb_coms -= 100;
            index += 100;

            if nb_coms == 0:
                return list_comments;
                
            if index < total_count:
                handle_number_comments(token, nb_coms, repo, total_count, nb_page, old_nb_repos, repos_name, index, list_comments);
            
            else:
                list_comments.extend(handle_api(token, repo, 100, nb_page)[0:total_count - index]);
                nb_coms -= total_count - index;
                index = 0;

                file_repos = open('../data/nb_repos', "r");
                nb_repo = int(file_repos.read());
                file_repos.close();

                if nb_repo != 99:
                    new_nb_repos = old_nb_repos + 1;
                    new_repo = get_repos_name.main(token, new_nb_repos, repos_name)[old_nb_repos];

                    new_total_count = get_number_comments.main(token, new_repo);
                    new_nb_page = 1;

                    handle_number_comments(token, nb_coms, new_repo, new_total_count, new_nb_page, new_nb_repos, repos_name, index, list_comments);

                else:
                    return list_comments;

                
        
        else:
            if nb_coms == 0:
                return list_comments;

            else:
                if nb_coms < total_count:
                    list_comments.extend(handle_api(token, repo, 100, nb_page)[0:nb_coms]);
                else:
                    list_comments.extend(handle_api(token, repo, 100, nb_page)[0:total_count - index]);
                    nb_coms -= total_count - index;
                    index = 0;

                    file_repos = open('../data/nb_repos', "r");
                    nb_repo = int(file_repos.read());
                    file_repos.close();


                    if nb_repo != 99:
                        new_nb_repos = old_nb_repos + 1;
                        new_repo = get_repos_name.main(token, new_nb_repos, repos_name)[old_nb_repos];

                        new_total_count = get_number_comments.main(token, new_repo);
                        new_nb_page = 1;

                        handle_number_comments(token, nb_coms, new_repo, new_total_count, new_nb_page, new_nb_repos, repos_name, index, list_comments);

                    else:
                        return list_comments;

    return list_comments;
                             
def handle_api(token: str, repo: str, nb_coms: str, nb_page: int) -> list:
    response = query_api(token, repo, nb_coms, nb_page);
    status = response.status_code;

    if response:
        response_json = response.json();

        # get_headers(response);
        list_comments = get_comments(response_json);

        return list_comments;

def get_headers(response: object):
    """Function that gets and shows the interesting fields of the header

    Args:
        response (object): response from the query
    """    

    # -- All the header
    response_header = response.headers;

    # -- Only the wanted fields
    response_header_ratelimit = response.headers['x-ratelimit-remaining'];
    response_header_expiration = response.headers['github-authentication-token-expiration'];

    print('>>> Token');
    print(f'RateLimit: {response_header_ratelimit}');
    print(f'Token expiration : {response_header_expiration}');

def get_comments(response_json: object) -> list:
    """Function that gets, shows and returns the comments

    Args:
        response_json (object): response of the API in JSON format

    Returns:
        list_comments (list): list of comments
    """

    list_comments = list();
    list_comments.clear();

    for line_response in response_json:
        comment = line_response['body'];
        list_comments.append(comment)

    return list_comments;


# ---------- Main ----------

def main(github_token, nb_coms, repo, total_count, old_nb_repos, repos_name):
    """Main function to call"""    

    nb_page = 1;
    index = 0;
    list_comments = list();

    return handle_number_comments(github_token, nb_coms, repo, total_count, nb_page, old_nb_repos, repos_name, index, list_comments);


if __name__ == "__main__":
    main(github_token, nb_coms, repo, total_count, old_nb_repos, repos_name);
